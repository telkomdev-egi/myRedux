import React from 'react'
import { AppRegistry } from 'react-native';
import { Provider } from 'react-redux';
import configureStore from './src/redux/store/configureStore';
import App from './App';

const store = configureStore();

const myRedux = () => (
    <Provider store={store}>
        <App />
    </Provider>
)

AppRegistry.registerComponent('myRedux', () => myRedux);
