import { combineReducers } from 'redux'
import people from './modules/people/reducer'
import myproduct from './modules/product/reducer'
import users from './modules/randomUser/reducer'

const rootReducer = combineReducers({
    people,
    myproduct,
    users
})

export default rootReducer