import { FETCHING_PRODUCT, FETCHING_PRODUCT_SUCCESS, FETCHING_PRODUCT_FAILURE } from './constants'

export function fetchProductFromAPI() {
    var formBody = {
        'group': 'KREDIT'
    };

    return (dispatch) => {
        dispatch(getProduct())
        fetch("https://prodapi-app.tmoney.co.id/api/get-product", {
            method: 'POST',
            headers: {
                'Content-Type': 'application/x-www-form-urlencoded;charset=UTF-8'
            }
        })
            .then(data => data.json())
            .then(json => {
                // alert(JSON.stringify(json))
                dispatch(getProductSuccess(json))
            })
            .catch(err => {
                dispatch(getProductFailure("error cuy"))})
    }
}

export function getProduct() {
    return {
        type: FETCHING_PRODUCT
    }
}

export function getProductSuccess(data) {
    return {
        type: FETCHING_PRODUCT_SUCCESS,
        data,
    }
}

export function getProductFailure(err) {
    return {
        type: FETCHING_PRODUCT_FAILURE,
        err
    }
}