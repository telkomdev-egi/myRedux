import { FETCHING_PRODUCT, FETCHING_PRODUCT_SUCCESS, FETCHING_PRODUCT_FAILURE } from './constants'

const initialState = {
    products: [],
    isFetchings: false,
    error: false,
    info : 'erroe cuy'
}

export default function peopleReducer(state = initialState, action) {
    switch (action.type) {
        case FETCHING_PRODUCT:
            return {
                ...state,
                isFetchings: true,
                products: []
            }
        case FETCHING_PRODUCT_SUCCESS:
            return {
                ...state,
                isFetchings: false,
                products: action.data
            }
        case FETCHING_PRODUCT_FAILURE:
            return {
                ...state,
                isFetchings: false,
                error: true,
                info : action.err
            }
        default:
            return state;
    }
}