import { FETCHING_USER, FETCHING_USER_SUCCESS, FETCHING_USER_FAILURE } from './constants'

const initialState = {
    users: [],
    isFetchings: false,
    error: false,
    info : 'erroe cuy'
}

export default function peopleReducer(state = initialState, action) {
    switch (action.type) {
        case FETCHING_USER:
            return {
                ...state,
                isFetchings: true,
                users: []
            }
        case FETCHING_USER_SUCCESS:
            return {
                ...state,
                isFetchings: false,
                users: action.data
            }
        case FETCHING_USER_FAILURE:
            return {
                ...state,
                isFetchings: false,
                error: true,
                info : action.err
            }
        default:
            return state;
    }
}